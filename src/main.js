import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import './assets/scss/main.scss'
import MoneySpinner from 'v-money-spinner'
// import './plugins/vuetify-money.js'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(MoneySpinner)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
