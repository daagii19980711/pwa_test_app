import Vue from 'vue'
import VueRouter from 'vue-router'
import MainLayout from '../views/layout/mainLayout.vue'
import PaymentValueComponent from '../components/PaymentComponent.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: MainLayout,
    redirect: '/payment',
    // meta: { requiresAuth: true, requiresRole: ['admin', 'operator'] },
    children: [
      {
        path: '/payment',
        name: 'PaymentValueComponent',
        component: PaymentValueComponent
        // meta: { requiresAuth: true, requiresRole: ['admin', 'operator'] }
      }
    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
